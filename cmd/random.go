package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

// randomCmd represents the random command
var randomCmd = &cobra.Command{
	Use:   "random",
	Short: "Get a random dad joke",
	Long: `This command fetches a random dad joke from the icanhazdadjoke site`,
	Run: func(cmd *cobra.Command, args []string) {
		jokeTerm, _ := cmd.Flags().GetString("term")

		if jokeTerm != "" {
			getRandomJokeWithTerm(jokeTerm)
		}else{
			getRandomJoke()
		}

	},
}

func init() {
	rootCmd.AddCommand(randomCmd)

	// add flags to our cmd
	randomCmd.PersistentFlags().String("term", "", "A serach term for a dad joke")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	//secretsCmd.Flags().BoolP("file", "f", false, "used to create secrets env file")
}

type Joke struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

type SearchResults struct {
	Results        json.RawMessage `json:"results"`
	SearchTerm     string `json:"search_term"`
	Status	 	   int `json:"status"`
	TotalJokes     int `json:"total_jokes"`
}

func getRandomJoke(){
	url := "https://icanhazdadjoke.com/"
	responseBytes := getJokeData(url)
	joke := Joke{}

	if err := json.Unmarshal(responseBytes,&joke); err != nil {
		log.Printf("Could not unmarshall response - %v", err)
	}

	fmt.Println(string(joke.Joke))
}

func getRandomJokeWithTerm(jokeTerm string){
	total, results := getJokeDataWithTerm(jokeTerm)
	randomiseJokeList(total, results)
	//fmt.Println(results)
}

func getJokeDataWithTerm(jokeTerm string)(totalJokes int, jokeList []Joke){
	url := fmt.Sprintf("https://icanhazdadjoke.com/search?term=%s", jokeTerm)
	responseByte := getJokeData(url)
	jokeListRaw := SearchResults{}

	if err := json.Unmarshal(responseByte, &jokeListRaw); err != nil {
		log.Printf("Could not unmarshall reponseBytes - %v", err)
	}

	jokes := []Joke{}
	if err := json.Unmarshal(jokeListRaw.Results, &jokes); err !=nil {
		log.Printf("couldnt unmarshal jokeListraw - %v ", err)
	}

	return jokeListRaw.TotalJokes, jokes
}

func randomiseJokeList(length int, jokelist []Joke){
	rand.Seed(time.Now().Unix())

	min := 0
	max := length - 1

	if length <= 0 {
		err := fmt.Errorf("No jokes found with this term !!!")
		fmt.Println(err.Error())
	}else {
		randomNum := min + rand.Intn(max-min)
		fmt.Println(jokelist[randomNum].Joke)
	}
}

func getJokeData(baseAPI string) []byte {

	request, err := http.NewRequest(
			http.MethodGet,
			baseAPI,
			nil,
		)

	if err != nil {
		fmt.Printf("Could not request a dadjoke - %d", err)
	}

	request.Header.Add("Accept", "application/json")
	request.Header.Add("User-Agent", "Dadjoke CLI (github.com/example/dadjoke")

	response,err := http.DefaultClient.Do(request)
	if err != nil {
		log.Printf("couldnt make a request - %v", err)
	}

	responseBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Printf("couldnt read response body - %v", err)
	}

	return responseBytes
}
