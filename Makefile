VERSION=v1.0.4
PATH_BUILD=dist/
FILE_COMMAND=dadjokes
FILE_ARCH=linux

clean:
	@rm -rf ./dist

commit:

#	git pull
#	git pull --recurse-submodules origin master
#	git add .; git commit -am "Kratos power";git push
	git add -A
#	git commit -m "$(curl -s http://whatthecommit.com/index.txt)"
	git commit -m "testing homebrew releaser"
	git push
	git tag -a $(VERSION) -m "pushing releaser taps"
	git push origin $(VERSION)